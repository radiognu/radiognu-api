#!/usr/bin/python
import sys
import logging

# DESCOMENTA LAS SIGUIENTES LÍNEAS SI QUIERES TRABAJAR EN UN VIRTUALENV
#activate_this = '/ruta/hacia/virtualenv/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/ruta/hacia/la/api/")

from api import app as application
application.secret_key = 'Agrega tu clave secreta'