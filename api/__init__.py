#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  radiognu-api - Versión en Python de la API de RadioÑú.
#
#  Versión 3.0.0
#
#  Copyleft 2014 - 2017 Felipe Peñailillo (@breadmaker) <breadmaker@radiognu.org>
#
#  Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
#  los términos de la Licencia Pública General GNU tal como se publica por
#  la Free Software Foundation; ya sea la versión 3 de la Licencia, o
#  (a su elección) cualquier versión posterior.
#
#  Este programa se distribuye con la esperanza de que le sea útil, pero SIN
#  NINGUNA GARANTÍA; sin incluso la garantía implícita de MERCANTILIDAD o
#  IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública
#  General de GNU para más detalles.
#
#  Debería haber recibido una copia de la Licencia Pública General de GNU
#  junto con este programa; de lo contrario escriba a la Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, EE. UU.

import os, sqlite3, string, random, json, gzip, requests, base64
from flask import Flask, request, send_from_directory, make_response, \
    render_template, after_this_request, abort, send_file
from datetime import datetime
from io import BytesIO
from PIL import Image
from functools import wraps, update_wrapper
from operator import itemgetter
from itertools import groupby
from raven.contrib.flask import Sentry
from config import ADMINS, DATABASE, SENTRY_DSN

app = Flask(__name__)
# Definimos que la aplicación será monitoreada por Sentry
sentry = Sentry(app, dsn=SENTRY_DSN)

# Agrega soporte para envío de errores por correo electrónico
if not app.debug:
    import logging
    from logging.handlers import SMTPHandler
    mail_handler = SMTPHandler('127.0.0.1',
                               'api-errors@radiognu.org',
                               ADMINS, 'Fallo de API')
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)

# Redirecciona favicon.ico por un archivo de mejor formato favicon.png
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.png', mimetype='image/png')

# Definimos el generador aleatorio de caracteres
def id_generator(size=8, chars=string.ascii_lowercase + string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

# Definimos las cabeceras de respuesta para json (sin caché)
def uncachedjsonheaders(view):
    @wraps(view)
    def global_headers(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        # Cabeceras para permitir acceso a la API desde cualquier parte
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        # Cabeceras para prevenir el cacheado de los resultados de la API, esto
        # se regulará de mejor forma en versiones posteriores
        response.headers['Last-Modified'] = datetime.utcnow().strftime("%a, %d %b %Y %X GMT")
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        # Se indica que siempre se devolverá un JSON, para su correcto manejo
        # por procesadores de los datos como por ejemplor parseJSON() de jQuery
        # o plugins de navegadores como JSONView de Firefox
        response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        # Agregando una ID de Transacción (que se usará a futuro), que servirá
        # para identificar internamente una llamada a la API, con propósitos de
        # regular el eventual abuso que pudiese tener el servicio.
        response.headers['Transaction-ID'] = id_generator()
        return response
    return update_wrapper(global_headers, view)

# Definimos las cabeceras de respuesta para multimedios
def mediaheaders(view):
    @wraps(view)
    def global_headers(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        # Cabecera para definir una edad máxima de la cache de 24 horas
        response.headers['Cache-Control'] = 'max-age=86400'
        response.headers['Transaction-ID'] = id_generator()
        return response
    return update_wrapper(global_headers, view)

# Definimos las cabeceras de respuesta para json
def jsonheaders(view):
    @wraps(view)
    def global_headers(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        response.headers['Cache-Control'] = 'max-age=86400'
        response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        response.headers['Transaction-ID'] = id_generator()
        return response
    return update_wrapper(global_headers, view)

# Definimos las cabeceras de respuesta para las páginas de error
def errorheaders(view):
    @wraps(view)
    def global_headers(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        response.headers['Cache-Control'] = 'Cache-Control: max-age=31556926'
        response.headers['Transaction-ID'] = id_generator()
        return response
    return update_wrapper(global_headers, view)

# Definimos la respuesta comprimida
def gzipped(f):
    @wraps(f)
    def view_func(*args, **kwargs):
        @after_this_request
        def zipper(response):
            accept_encoding = request.headers.get('Accept-Encoding', '')
            if 'gzip' not in accept_encoding.lower():
                return response
            response.direct_passthrough = False
            if (response.status_code < 200 or
                response.status_code >= 300 or
                'Content-Encoding' in response.headers):
                return response
            gzip_buffer = BytesIO()
            gzip_file = gzip.GzipFile(mode='wb', fileobj=gzip_buffer)
            gzip_file.write(response.data)
            gzip_file.close()
            response.data = gzip_buffer.getvalue()
            response.headers['Content-Encoding'] = 'gzip'
            response.headers['Vary'] = 'Accept-Encoding'
            response.headers['Content-Length'] = len(response.data)
            return response
        return f(*args, **kwargs)
    return view_func

# Factoría personalizada que devuelve un diccionario como resultado de
# consultas a la base de datos
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

# Método que se encarga de modificar el tamaño del arte de portada
def resize_cover(path, size, as_base64=True):
    image = Image.open(path)
    image = image.convert('RGB')
    if size:
        # Acotamos primero el tamaño
        if size < 50:
            size = 50
        elif size > 400:
            size = 400
        # Luego preguntamos si es necesario reducir la imagen
        if size < 400:
            image.thumbnail((size, size), Image.ANTIALIAS)
    # Guardamos los cambios
    thumb = BytesIO()
    image.save(thumb, format="PNG", optimize=True)
    thumb.seek(0)
    if as_base64:
        return "data:image/png;base64," + str(base64.b64encode(thumb.getvalue()))
    else:
        return thumb

# Nos conectamos a la base de datos y configuramos el cursor.
conn = sqlite3.connect(DATABASE)
conn.text_factory = str
conn.row_factory = dict_factory
cur = conn.cursor()

@app.route('/', methods=['GET'])
@uncachedjsonheaders
@gzipped
def index():
    try:
        now_playing = requests.get("http://localhost:8000/json2.xsl")
    except Exception as e:
        return json.dumps({"error": "No se pudo conectar al servidor Icecast."}, separators=(',', ':')), 500
    if now_playing:
        icecast_data = now_playing.json()
        if len(icecast_data) > 0:
            # Inicializando variables
            listeners = 0
            isLive = False
            broadcaster = ""
            show = ""
            artistName = request.args.get("artist")
            songTitle = request.args.get("title")
            for item in icecast_data:
                if item["mount"] == "/radiometagnu.ogg" and \
                (artistName == None and songTitle == None):
                    artistName = item["artist"]
                    songTitle = item["title"]
                elif item["mount"] == "/envivo.ogg":
                    isLive = True
                    broadcaster = item["broadcaster"]
                    show = item["show"]
                listeners += item["listeners"]
            if not artistName:
                tmp = songTitle.split(" - ")
                if len(tmp) == 1:
                    songTitle = tmp[0]
                    artistName = "Artista desconocido"
                else:
                    artistName = tmp[0]
                    songTitle = tmp[1]
        else:
            return json.dumps({"error": "La radio no funciona en estos momentos, intente más tarde."}, separators=(',', ':')), 500
    cur.execute("SELECT ar.artist_name AS artist, s.song_name AS title,"
                " al.album_name AS album, s.song_id AS id, al.album_id,"
                " al.album_path AS path, g.genre_name AS genre, c.country_name"
                " AS country, al.album_year AS year, al.album_url AS url,"
                " l.license_url, l.license_name, l.license_shortname,"
                " s.song_duration AS duration FROM artist AS ar, song AS s,"
                " album AS al, genre AS g, country AS c, license AS l WHERE"
                " ar.artist_name = ? AND s.song_name = ? AND s.artist_id ="
                " ar.artist_id AND al.album_id = s.album_id AND al.genre_id ="
                " g.genre_id AND ar.country_id = c.country_id AND s.license_id ="
                " l.license_id", (artistName, songTitle))
    db_data = cur.fetchone()
    if db_data:
        if request.args.get("no_cover") != None:
            cover = ""
        else:
            try:
                cover = resize_cover(os.path.join(db_data["path"], "Cover.png"),
                    int(request.args.get("img_size")))
            except Exception:
                cover = resize_cover(os.path.join(db_data["path"], "Cover.png"),
                    400)
        result = {"artist": db_data["artist"],
                  "title": db_data["title"],
                  "album": db_data["album"].decode("utf-8"),
                  "id": db_data["id"],
                  "album_id": db_data["album_id"],
                  "cover": cover,
                  "genre": db_data["genre"],
                  "country": db_data["country"],
                  "year": db_data["year"],
                  "url": db_data["url"],
                  "duration": db_data["duration"],
                  "license": {
                      "url": db_data["license_url"],
                      "name": db_data["license_name"],
                      "shortname": db_data["license_shortname"]
                  },
                  "listeners": listeners,
                  "isLive": isLive}
        if isLive:
            result["broadcaster"] = broadcaster
            result["show"] = show
        return json.dumps(result, separators=(',', ':'))
    else:
        if request.args.get("no_cover") != None:
            cover = ""
        else:
            try:
                cover = resize_cover(os.path.join(app.root_path,
                    'static/radiognu.png'), int(request.args.get("img_size")))
            except Exception:
                cover = resize_cover(os.path.join(app.root_path,
                    'static/radiognu.png'), 400)
        result = {"artist": artistName,
                  "title": songTitle,
                  "album": "RadioÑú",
                  "id": 0,
                  "album_id": 0,
                  "cover": cover,
                  "genre": "",
                  "country": "Planeta Tierra",
                  "year": datetime.now().strftime("%Y"),
                  "url": "http://radiognu.org",
                  "duration": 0,
                  "license": "",
                  "listeners": listeners,
                  "isLive": isLive}
        if isLive:
            result["broadcaster"] = broadcaster
            result["show"] = show
        return json.dumps(result, separators=(',', ':'))

@app.route('/catalog/', methods=['GET'])
@jsonheaders
@gzipped
def get_catalog():
    # Primero obtenemos los datos que necesitamos. Los artistas pueden ser más
    # de uno, por lo que la consulta devuelve un resultado duplicado por cada
    # artista distinto en un álbum, es decir, si álbum tiene 4 artistas
    # distintos, se devolverán 4 resultados para ése álbum, cada uno con un
    # nombre de artista distinto
    cur.execute("SELECT al.album_id AS id, al.album_name AS name,"
                " ar.artist_name AS artists, al.album_year AS year,"
                " al.album_url AS url, g.genre_name AS genre, c.country_name AS"
                " country FROM album AS al, artist AS ar, song AS s, genre AS"
                " g, country AS c WHERE s.artist_id = ar.artist_id AND"
                " s.album_id = al.album_id AND al.genre_id = g.genre_id AND"
                " c.country_id = ar.country_id GROUP BY al.album_id,"
                " s.artist_id ORDER BY LOWER(al.album_name)")
    db_res = cur.fetchall()
    getAlbum = itemgetter('name')
    catalog = []
    is_first = False
    for key, album in groupby(db_res, getAlbum):
        is_first = True
        entry = {}
        for record in album:
            if is_first:
                for item in record:
                    if item == "artists":
                        entry[item] = [record[item]]
                    else:
                        entry[item] = record[item]
                is_first = False
            else:
                entry["artists"].append(record["artists"])
        catalog.append(entry)
    return json.dumps(catalog, separators=(',', ':'))

@app.route('/album/<int:album_id>/', methods=['GET'])
@jsonheaders
@gzipped
def get_album(album_id):
    cur.execute("SELECT s.song_id AS id, s.song_name AS name, a.artist_name AS"
                " artist, l.license_shortname AS license FROM song AS s, artist"
                " AS a, license AS l WHERE s.artist_id = a.artist_id AND"
                " s.license_id = l.license_id AND s.album_id = ?", (album_id,))
    response = cur.fetchall()
    if response != None:
        return json.dumps(response, separators=(',', ':'))
    abort(400)

@app.route('/search/<term>/', methods=['GET'])
@jsonheaders
@gzipped
def search(term):
    if len(term) >= 3:
        # Canciones
        cur.execute("SELECT s.song_name AS name, al.album_id AS id,"
                    " ar.artist_name AS artist FROM song AS s, album AS al,"
                    " artist AS ar WHERE LOWER(s.song_name) LIKE LOWER(?) AND"
                    " s.album_id = al.album_id AND s.artist_id = ar.artist_id",
                    ("%" + term + "%",))
        results = { "songs" : cur.fetchall() }
        # Artistas
        cur.execute("SELECT DISTINCT(ar.artist_name) AS name, c.country_name"
                    " AS country FROM artist AS ar, country as c WHERE"
                    " LOWER(ar.artist_name) LIKE LOWER(?) AND ar.country_id"
                    " = c.country_id", ("%" + term + "%",))
        results["artists"] = cur.fetchall()
        # Países
        cur.execute("SELECT c.country_name AS name FROM country as c WHERE"
                    " LOWER(c.country_name) LIKE LOWER(?)", ("%" + term + "%",))
        results["country"] = cur.fetchall()
        # Géneros
        cur.execute("SELECT genre_name AS name FROM genre WHERE"
                    " LOWER(genre_name) LIKE LOWER(?)", ("%" + term + "%",))
        results["genres"] = cur.fetchall()
        for i, genre in enumerate(results["genres"]):
            cur.execute("SELECT album_id AS id FROM album WHERE genre_id ="
                        " (SELECT genre_id FROM genre WHERE genre_name = ?)",
                        (genre["name"],))
            results["genres"][i]["albums"] = cur.fetchall()
        # Años
        cur.execute("SELECT DISTINCT album_year AS year FROM album WHERE"
                    " album_year LIKE ?", ("%" + term + "%",))
        results["years"] = cur.fetchall()
        for i, year in enumerate(results["years"]):
            cur.execute("SELECT album_id AS id FROM album WHERE album_year = ?",
                        (year["year"],))
            results["years"][i]["albums"] = cur.fetchall()
        return json.dumps(results, separators=(',', ':'))
    else:
        return json.dumps([])

@app.route('/preview/<int:song_id>/', methods=['GET'])
@jsonheaders
@gzipped
def get_preview(song_id):
    cur.execute("SELECT song_path AS path FROM song WHERE song_id = ?",
        (song_id,))
    response = cur.fetchone()
    if response != None:
        preview_file = open(response["path"].replace(".ogg", "_preview.ogg"), 'rb')
        preview = "data:audio/ogg;base64,{0}".format(preview_file.read().encode("base64").replace("\n", ""))
        preview_file.close()
        return json.dumps({"preview": preview}, separators=(',', ':'))
    abort(400)

@app.route('/preview/<int:song_id>.ogg', methods=['GET'])
@mediaheaders
@gzipped
def get_preview_file(song_id):
    cur.execute("SELECT song_path AS path FROM song WHERE song_id = ?",
        (song_id,))
    response = cur.fetchone()
    if response != None:
        return send_file(response["path"].replace(".ogg", "_preview.ogg"))
    abort(400)

@app.route('/cover/<int:album_id>/', methods=['GET'])
@jsonheaders
@gzipped
def get_cover(album_id):
    cur.execute("SELECT album_path AS path FROM album WHERE album_id = ?", (album_id,))
    response = cur.fetchone()
    if response != None:
        path = response["path"].decode('utf-8')
        path = os.path.join(path, "Cover.png")
        if request.args.get("img_size"):
            cover = resize_cover(path, int(request.args.get("img_size")))
        else:
            cover = resize_cover(path, 400)
        return json.dumps({"cover": cover.replace("\n", "")}, separators=(',', ':'))
    else:
        if request.args.get("img_size"):
            cover = resize_cover(os.path.join(app.root_path,
                    'static/radiognu.png'), int(request.args.get("img_size")))
        else:
            cover = resize_cover(os.path.join(app.root_path,
                    'static/radiognu.png'), 400)
        return json.dumps({"cover": cover.replace("\n", "")}, separators=(',', ':'))
    abort(400)


@app.route('/cover/<int:album_id>.png', methods=['GET'])
@mediaheaders
@gzipped
def get_cover_file(album_id):
    cur.execute("SELECT album_path AS path FROM album WHERE album_id = ?", (album_id,))
    response = cur.fetchone()
    if response != None:
        path = response["path"].decode('utf-8')
        path = os.path.join(path, "Cover.png")
        if request.args.get("img_size"):
            cover = resize_cover(path,
                int(request.args.get("img_size")), False)
            return send_file(cover, mimetype='image/png')
        else:
            return send_file(path)
    else:
        if request.args.get("img_size"):
            cover = resize_cover(os.path.join(app.root_path,
                    'static/radiognu.png'), int(request.args.get("img_size")),
                     False)
        else:
            cover = resize_cover(os.path.join(app.root_path,
                    'static/radiognu.png'), 400, False)
        return send_file(cover, mimetype='image/png')
    abort(400)

@app.errorhandler(404)
@errorheaders
@gzipped
def show_404(e):
    return render_template('404.html'), 404

@app.errorhandler(400)
@errorheaders
@gzipped
def show_400(e):
    return render_template('400.html'), 400

@app.errorhandler(500)
@errorheaders
@gzipped
def show_500(e):
    return render_template('500.html'), 500

if __name__ == '__main__':
    app.run()
