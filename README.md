# API pública de RadioÑú #

Bienvenid@s a la documentación de la API pública de RadioÑú, aquí te indicaremos
la información que obtendrás de ella.

# IMPORTANTE #

La API ahora se utiliza por **https://api.radiognu.org/**. La antigua URL
https://radiognu.org/api/ **se ha descontinuado y ya no es accesbile.
Es necesario actualizar a la nueva URL.**

## ¿Para qué rayos me sirve una API? ##

Si eres desarrollador, una API te ayuda montón cuando deseas obtener la
información que necesitas para la interfaz, aplicación o servicio en el que
trabajas.

## ¿Cuál es el formato de los datos? ##

Como en la mayoría de las APIs conocidas, usamos el estándar JSON para el
transporte de datos. Más información en la
[Wikipedia](https://es.wikipedia.org/wiki/Json).

## ¿Cuáles son los datos que recibo? ##

Aquí hay un ejemplo de los datos que recibes desde la API al hacer una llamada
simple:

```json
{
    "album_id": 172,
    "duration": 221.1004081632653,
    "year": 2012,
    "genre": "Electro Rock Folk Fusion",
    "id": 1426,
    "album": "Bailando con los Gallos",
    "license": {
        "url": "http://creativecommons.org/licenses/by-sa/3.0/",
        "shortname": "CC BY-SA 3.0",
        "name": "Creative Commons Atribución-CompartirIgual 3.0 Unported"
    },
    "artist": "El Pacto",
    "url": "http://elpacto.org.ve/descargas.html",
    "country": "Venezuela",
    "title": "Pueblo a la Calle",
    "cover": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR/mAAAY8klEQVR42gXBd5Dsd2EY8G/79e3ldm/v9uq71+71ptfUJZBAiFgIMhNMwAZDJo7DMA4ehwkEk4n/YlwnzDgT49geO3EGEEhCDTVUkF7R67p3926v7u7tbd/9/fZXvy2fD6ye/lTEmQe4BABIqUoIhTSg7DKmIaIKiDHgEnAmFEViiAgVEZRAMksqCDLJJJQASg45AzQkpok4cZEQQGCBIARJITljNkUpE4soYlJIgCSAGoAUcowRFDBCHEoSKYhg4GEyUCDRovA7Gq9BlqeAE9QXrCfBY+PmeAj+pRNIBUIFnAIIM/peEH5ejz9A0DqS1yK6hEIBFawKiBWFmLpmTUwWt5duh5x9QVPOuxBC9jaSL0r+qYXkwYT860vtM4VEEWMomEble75/IBMf+kyGbLKcAyDSuay4/FggHwg4/sOxqTGsHtDwNzQy5cMkhgYUGy6dV/E8l1UZliX8JNDvQf7pw+NfrjrxKIoj9FRMTUbyJ4APMCFzi11i9S3rt7//3Z9euZTptP4zVTagDJJkYR4OXflD2/vCZy7UVxp/LcJJVS174F1Vvq2KjCS/4eFNRQ69gA7DzUC8jmVcirMckRCRD1F0m4nxPaljN+0ZBRFVVYfyZ1E0htEfgBgSVLDo13T0R48/ad79VactdcIvC/HpAFzD0a3J/NDuqFRq2Lj83HMA4i8mFdiLLhvyoYhcqfL/SPVX3PZ6q/fDZHqx3r7fExkugE/PK8pFyiBS2xhuKzBpwrip6B6FEggO8R+Nzy0TmKSg0A6IKjq+VCnXGDgYiSQXCkA6lUmMbtPgPZ8en9ZwbxhH+qodlYSoCioOTcSLC0ywRDLV6g0TOPjGPrNzq9tP6Y8/oY4vsXsReCmFvtz0tVp3FqOUAmUGLhyR+wXCXRlKft1E/0jYSwj2ctr9ACh2uCAE/l5+Bodsl/D9IUaucIGoYbRpKQrC8RFNUZaDcNKPLgA12Ojo7aAAUWfAihDMAuUFC8xMmaXznz730P1Tc1OGaWbay8dPqeObdmXEfjqMbA8vafJoCB8fMqYgTVcAxK7GBw5r70A1ZaV0kOrQiyP+MFa6Le8SBAoXpwTGf1ic3dDEcVd2Ff5WSq2X9LzGZJfGVCA0aSKkUsERzhn4goVKBlMIT1FQguj1MHwvTz61V7veU4Vq3L5y4+by2gnc1HmonrUOD/vpDQUI+EiIHgTIEQJD6FC+y4AzYuEIrZPwBzSsTMYuPpzPmVpvo7dIDBshBcljgODvjs2oVD6vcvX8mD6pn396ZqM9XLHMyYdytTntHSsm9qhwUvFy3N2vBQdweJ8OkfK/qv7KKeOzR+ODQX/6vs9EidyNj27tmyicje+O7FDEx8ZPLiaub45LNSak6we7nPtJvarguzGtQlDiwp69C6W0xX90q1k6Oja6sMcsF+9W69/lwQwmj1OIv1Oa/6Y/mH1q/IEjZjqtJq2UC8OFxYxggphMZMmOoil54GVRrwjbugrGS7eWvaWU8qffP8FQiGN4qjzXUOadofeDL+8Nh1eKCxMxzRw789TV3tY3b2184qsn7WO4d7b4AdZvDaNNGc4fmcoeLL/f6585Nj4Jgs6AVF7b2ObomSdPb9+450n0NED4d/Llly34pUfzOIkIwQwUFdaGKo0gN5BTAMwELg77seL+2OS5eDpBjLRvD5/4V3sxZZFApi7jsH3jytUyqRXJema2MH8iHzm9+tb1Go1dmNXNWTCaNoeJnKmq0xPx86fVxWP7eFCIqbWdld0jRxNUYN5RYDcS+/ec00RttfGAZuJvZku/xHx/MdFvw5tLvpYM1cm86wd6PBWbuMiwkZk/mp6aQ+Z4rZfJTJ2gIUjpu9PTmfU6W6l4pfnz222WnkoX91umktrcSPbFxPjCI63tHXvoZBcyb7zno+yCGjOjCLk96pN4vTHsiDYHypACkEKbu9IwzexUCnuD3rBV2/UfRRrcWXzghSPGvosZPhx4AgslmZ0+jhBX9VR8/IggppbcIwHz2iuBN1Ixz+UyigX+2w9+pMVKnAVWXCecV7eqp+/bNxz2li4tc2ssmU0nvJ0zWS999Fy6vHf30t2tdz4KcDCCgjZcZGgAA6/vZScTCyfSq3e9qcO59lYn7oNr3e5kj/6+r+BvT+yznpo3x0hh8Qlt7FjIUsmErifmuo5oNxsc6JVLr997/+W4Kab3L1aXrlTXN7uD4PkX3y1n9U6zeev2XctUbYackby7tFGYmhh223furqqI5TJZmZqaj1UVsCuCaZWTza3tHRFURk49GG2yILC9fWMWUqTdo8OOv7LTAcnE4+VEZmsE6/c91fmTr8ezZrPrWak8BixbmKw1ZTKRYqGTG0+qJOrutjwP1O99OOq35w8dlSyMgrAwXmo7wVvPv5zHPopbXn/o9Ad7i9B2w+1Nd/LwIvAGM9OJXNIzE3O5/Rea282f/ulftTzu5gzF1DWT+CAoQ2Usab773vrhTGJmJmbMThSq1Zm32vhrifyfXb2SG8tMzM6qutJrNl796U8yllLfuN1qjJo74Opv7nXakWYYWiylaWhnq1qtrPpcIIwFpXZrx7UHmbiKiJQSicAzLSNZNEMv2G0MIQWjARzacG35Q8G6XsgskDxxYvH8g6fOnr4Yoez84eKBg+V0rpAax0BTq67rrtsHBhB/a3w//NdPlqfy1eWV21evdVuRxDgYVu0oeuEXr2S0wuWr/TfeX1v6YEV6ViZjDxrrNlOIZiIp6+tr65X1tfVaFEGrML1Za1e27V3PVEoLSmL8xGOP1VypFGYafnD5xo7foaXDB/uYfVxt3L27aelwfu9MtWZvrLVCZDW7Xjo+8+alBumNzkOAf784uzpd3Lh3p7ZagdxRiTGx9+Tq2nqZyo3m9pvXXipZ3uTshSiClUq7trEd14f1tXs6p3Ed37xbSSTSn/vyVx545nOxuPHMV7509uGL5fn5VCaTTqobyxXTMk5fvE8ydvD4ockjhxVdXzx1bMTgtavXduqNhBFNZtCl6xuQkL7LAwVNn1yYiSfnKhv462Pld8X4yYvnIEadfqLdqVtqdntrp7m24lD/Wr1T2V2Z0DM0KHb7HS/Md7tur7/rQrU5cPRU5rFPfRJgeOvSr1/853/54PK1dm2n2WwkY3qtttvp21NTxepG1W7U+9VaLGEVJ8YR53HE5hamsKbXa4Ne05lZ3Dc9XTp/snxgIXvw1Klc5OI3r8K7i/e/NjtnKIqfP8zNKRNUIW9yYL/0wftbQz8CyGORLtDJmc9DPGMHI1UAjEBmXJ2Y3Wk2togSh0gddG27XRsFzkTO4hjosZyaKGNqA79Vudv65BPnr964kdbFw+cO91v9yHUPnT5cW2nsjILiXHnr2k2cTh44NFcoxbW4Gr1/q/zjK3Bp8ZFXyyUdUqucg7r68vWru44bSmCpeDjyA0bbfiSkIEjMjn0uaS4i6mgKoRwfO0yxudXRkiGYdjdReTJv91uTC17b7UW9AZTFXJweP1JaX65s1vtutqz5wGgtHy8nHKlOnjhkqejubz4auDSR0EeNTWjEE4W9bkRPyM1DP7lBKMLXBmxvOWWUzI/fucIoDQVknC7bTkAZkMIkigREAN4avRLSxsz445xF/shfdiY+8+wj5vbK8oo/dTYzkZm7WzF8vVIY2YUp/frSldPnHjZjxHb6rU59LntecJEcszoEZI6e0Me0rZrpyb4fwZ12NrTHLZDsVWOVfuCK7mmD4N/Zf/LWZ7+2cPa+ZFJP54ptH/ba1bYbMiHzlgIkFBAiKRAiAMJG6yZHFNEpK63NHSh3BrDXInd/tdHeHLU324oKj12Yh2Y6AmUhp5bXN371q+5SJUaAVl/9zdL6JXfkzR36FAvF1vt/W11tr+7Ut9rWViPoOEPbZu7Ab7osJ/iD9l2CEdp/+BirX/vwgw9L8bnx2KK2R2qN5d1uD0FFxdBnzA4pRiwOtVisUNl84/SZhaOPfrG1sRk0Oo2qHUJAuPSHXv3ebnO9P3sw7jYa25u3q4M6CBUBetutQIIOFGGztyy4Ox/LZLVNLDdUJ+25xFBNxkUYRhH3OUkohAKJ8Jcz869GRStVaNrxaqVPkUbs9a1BO2KKqqKBG4SMHZkdUxBq2Q5BeCZnRDBMg3iz3s1Mzcaal7KxlGEQh4FYwrQ77eHS243KCjWxRhs+oAqUkCgg0pMEJ5Jjht+e0Pr1TrA6CHu8f3SsjcNeb+TkCcpnyupEcSZBz+7eJJT1P/r1G2sfxieSbs9rV2obO3ZVk3ombg5dfn4ucWhv7laVrdfbmYRpQPDQyWK107z0zn9XURKGZxb5bnvwmion28aTmZlMMRNrdzLHji1mxZvJTv+eeKwtCpkxa3NjJ5dJpuIiFX7w8/ffaI+i3zuYCkPtzYbtjGrlZEpLjqXnciMbGKomEMb/fmLhZmF6j7nuOutJM2OgYRpBRc0ShewM7adPmF96BLW5d3edWwratQNvxAchX29xC7NuZ7Xhh+/3dpX0RGnfY3ERFQvqOXWzI6698+L/IYLH9j20//77qnfWAEUz+j1j62fPXb0msfjjhzPTceOF1XbP5xeni8emCoNQDrtrPs+fXsws3Hgf/4c9x/VPfj4Wxzs9c92ur7bWbCo9RvscHJ3E/+nZ7B/8ZbvykXMoS3xDmy3G79acRie0DIwg8iTpCf8Y0H5XsVRCPmoCtrtaufy3b965PmZYqelZjg/c/Gi9v9Eusnu9xvOvbHbiyfiPvzYhJP/Oq+2lAR1Jzqh3IKtG9lYU5apOVqttPeit4a+X927MLY6lF+qN1urWry1VtymPqxBEoZRIrdHdTWeEtD/5fGFuhoQD4EjZGYQIQyaEQhCS8AJRSWN9ur12p3C04QWX+TJSTSVN9s8kl9fKTl/qqmkmRpXeElP0H38793E9+MX7Yd6iGQ3P6NYz+/Pt9ihTGOtHihavZ5zwXNDG39636B4r1TdW72xc3jsxtdXZlTwyCcnGrMpwpG666yrQkurrm7hfC5+7tWNTMvJdBRIK/JDSkNoDLf5vLnzGmZseHf0spt3L115RSGyrHdhu+P3fE5BXU+lxv/Px0vrGwUVzs+ULW8E0Wu+IdiTdSLRGHCGomRbTLJwkKRyd6XQIkGBns971UoaRru/eKMdVia3WYNgb2RaRa2r0jUQSF/G3rq7U0ykP8QIRExNWLPGoSvLVxi+hyBbyD72Y2bNb6x20b2z7L3/x4fzROdP2kiEQ48a1Rw9r//OtuxSBLzyduVUXP3s72DcJYmZkZcmtFR8pcMIiOdOymWpqTMbnUijH4Qr+avnAzvEn/cZ25HYOLUx95vzTl5dvOJ6vIuD4/m8tnO5F5J+H5YXMsYP5UoptegiOKcYBUw6DhVLibFydS+hKSokyVoLw29/5wvKT5wrTYwCrmh6RVz+Qfdu6Vw1hnP/uF1PvfOAqBASC6wgNhsKN4HHLmNWMtYEfRT6PsKmM5Zxob3eN+KOQDdpHZycWx7kxferFV14oajxXiN3baTx64PGedtaEz5foA6EHOp3tqeTnZMiEltqK8ukUgQyUSqlk0kAgbHX6FO7b6l6/udkPfIA0dn0tMChCfnDiUPJ/v7z91Dd8Q8OJBLCglJCMYTWXiHSI19xBqphLKVNt94CC9sbyLXSHEyuhJNH61mbbGD+1/u5zCdKeWdgL0PT9J9OBsfjKry/b7TtjuTmD7O3SrGlY83mApB9LqgqKmq0BQmOQ03ptyHBMSP9HP5lPpG7eqTi/ddHQNLS07VtDGkKvH0YxVUjIuyN9QENk8oC7MZ2YWnI+vwdZB9vuFBVRM+A2BIgzEoWiEYTVzmw00O4/fHS3uULJgUzuRHWnPera+xaP4nuHtn0nxIqhC0Mz7FEEURxpZDAEMtTjSZVggpTUsB2pCo8ZB93owHfuu9mtXX9hh03nE81+tNN0ZsyMw+wzYweut7Ym8ofmx6eag5Tt6jEzxbVYGEIFBmYu3xXKcOgBRSG7/cGbNenR5ulEb6k22Jb7OgNLCz8e9RtuowagekDQZkj7uKpyt0s9CSFSRIJkpPS4qtZcJ6aoSlwMfDsKmcYcjMtRVo/Vgw/X1PEki/zWTGEilfmE7tVYMjVrHNShaePxAA+svA8TdhBzsYFqKw1LSTKrFDIHYEQ4jnbqd1MKWO1QgvWmbo7v8zrVSruyyqMoDKKSHrkoPQrbhK0h3dEtNeqNXGoCLkQkkQJFlemYA6BGlDIaSGfpH/vozMiYLO0nEbfyhUF/w8FvCbnH7g2QgkVwQ8cqhwpzfelCzjwtZvABIx4JRDHdGQJICIBERxgwUgtx5PtEXemAVNTvG8U57oSqvbPD7LSo2DyN0VAG3PMjyaLI9REyFaLwQECAPYLjJkoSRQIpiAqQ2QwdSRxVIdmENQvj6xMLfou6/YFq8rRGgUuHFIQuVE1dQYYWCSWGOYfCcwRzJJQEQcXIpQwFiq5nlYuknBtWG+apA7nyOGi43kp60N2lnfX0pOr2Y4IDK66x/gjENCOm0JGjUEFUwnXV6w+lomla2omshQVZwvrYUBdxfH+u/8tKPZCZRHwCIRUIZCMaeh5gTDF0iSMMFIxJKJASj2HVks4IA0ACHg3sviBa0PekgCKM1OmsP/Ds2zel5wkeaUARhdmgM1A1SJDCpQuUEHiMSVVGVOqWD6TsugLE7CCiAj426YS94f9ozAqkJqk/ium0eNISidDRsJLESRSFQnJFQC6JHjMwwRwgDbFYIm5SZsc0wiJEojBEPLQM1QM2oZwOqTLSjYDbhGkzCbfWtutVpEgFSgkNrkCFMmLiKAhpyAgw4mM5gSLq4JCKw/nUvMbDQffv3mtNlxQMzf5Q3jEPLX5yZufDy6K3aiOR0My+bY88ByJgccw8xAkS2PDCbpZqMWd35CFiJvCzmdI7Ca1UyIb2MBIyW8xFrbbDApLRwW5LBj4TPiIYIt0dejwKgpB2+30kCZNSEulGQeDapqFPZVM6B03H3YD5dEKXBLuuzVg/GnTalRUKIpyL+VSMfJoAEgCCLUMhAmmKLyBRiSQwp6ELChFUXsQKgQLY3d6uqgUMBP6wvcb0dIqubTIIkpmUH0Yq1qWgvtPjDPhhNG0o0cgNOShlklyGChemoU0hOnR6W6HAiZQ/6DkunVMN6feuEM0IXQ1pGSuHuRChzZHWZwyjSA84YHQAQErTjJA3pHyGgk2ftxUkASZEBZKJ/nY1kqCcyz4+bf3Xn18amxo3MGw3B9J3FQxczpBkioSUsixGkaXddgexAYjFE9IL+0JxosiTSugkYxEWyAyBW7Cddcan44RHIgq9lY2KYNyA0FJVqGsJK4k8f9b13gyDoGC1gvBZBvIU/7nG9uM4RohojKdN6IWMM77TC6eeefDf7TRvrHQClUAELCwPAwQkVgTKSJiBetyXaY/80kr+nTcyfJlJFagTxzLgbABBF+vWXk0/Wx8+4vGQmFQBlsR2SCtA3kLyFg4HYagEXr/dnYKgz6GvasUgejCgi4D8BQkbEB7koUAafHvh5D+ZouXTTU5HlHqUHh7P5UZ0PqCKAJMU7g1Fh8gqkW8g1pScReJ8MnWuMXjBhL8AkQl0BHDEqGFk8xMGoIO/YZLfG13Lm2+RyA85UPDDSJ+xI6IobSKGGHEhX1NZF4qUIDMClRQyxLgi6F6iVlX1sKZ+kxISIcLT6oTKkmEw8v0xzUg23SOAzPuQICAo+FAXjgL+L4+kQr737SdtyBoBp5B/O6le9NRXX7t3/sw+Xcc372wdOTY5ZsL8R1tXv5jFce2rhPeG8L/8+M22lN964uCVt25msAmBNCD4t4EChEAY3lBlW4qsJGexVoeSSjrPNA4wfiiR/0vfpQCUINkX0AACR0WIySyTa0TcNsAV5lYQ/NjSvpfLdTEfk7RQ7VVClu44H3ecatPJJLTQDXsjSXQjFc8IL9BVcGet3R/RXsjQRvf1jea5x+8727FfsEcqRDUF1gg4EAIsZEElusTPw/AdSbsYTGoxooITEpI8RscRGguia5xdw9KP6ADA+VIadYJV328n1B9+4sE/+5uX34sVT336zLvf/4e/z+gtyp5gyhrAfyEdbqjLFUdLFKxM8oPby7qMvvnggcIPfj4jlDcMXoLK6bj+Ulxdee5tQKGhwfFivNbo/0aIiVx8JFiNsjkAvsLV6wYCEjUktymCEOFnY+P1mK7raiIeDxnoM5ZOxP/4gX337m2/hMAWMNCxuadPzLzw+iV67OBvnyieqvSngVZWlX+IAzMdJ5z6hi551GlsZ4iLers3Ot199+21uqO8pvOY8v+i8Nji1CNj+p+33M1ELjlfOsyjPR75OXORYiQwNiQaKHgD8AZCUxMznx5P5Ds+/Lh0YsSoArEWgRAwIASBIJnUg4HXZ5wrqu37BxeKva3muoSzpRhpObpPHYS2ECwT0mfU5xxByCBKqMSEqO54E+MJEMp+EKoQjxg3TTMDoy5XEUT2yE2Y6gQAnZC1oFQQZBAJTdUhBCoKFaWctLKG+f8Bj5FK0A7QesEAAAAASUVORK5CYII=",
    "listeners": 33,
    "isLive": false
}
```

Vamos desglosando cada uno:

Las entradas indicadas en cursiva hacen referencia a *información incompleta*,
es decir, solo algunos álbumes incluyen los datos indicados, si no hay datos,
la API devuelve un string vacío para ese campo. Esta info es de acceso temprano,
y se está catalogando y agregando de a poco. Se anunciará cuando se haya
completado esta información.

* `artist` es el nombre del(los) artista(s) de la canción.
* `title` es el nombre del título de la canción.
* `album` es el nombre del álbum.
* `id` es el número identificador de la canción. Este dato es útil para el
argumento `preview`.
* `cover` es la imagen del arte de portada, codificada en base64. Este dato
puede variar en longitud, dependiendo del argumento usado cuando se llama a la
API.
* *`genre` es el género musical principal del álbum*.
* `country` es el nombre del país del(los) artista(s).
* `year` es el año de publicación del álbum.
* *`url` es la dirección web donde se puede obtener la descarga del álbum.*
* `license` contiene la información de licencia de la canción:
    * `name` es el nombre completo de la licencia, como está definida por la
misma.
    * `shortname` es el nombre corto o nombre código de la licencia, como está
definida por la misma.
    * `url` es la URL hacia la definición de la licencia usada para distribuir
la canción.
* `duration` es la duración estimada en segundos de la canción.
* `listeners` es la cantidad de ñuescuchas al momento de obtener la información.
* `isLive` indica si la radio está transmitiendo en vivo (un locutor conectado
haciendo su programa) o en “diferido” (término usado para indicar que es la
máquina la que está programando la música)

Adicionalmente, si se está transmitiendo en vivo, se incluyen los siguientes
campos al final:

```json
{
  "broadcaster": "Fulano de Tal",
  "show": "Fulaneando... y tal"
}
```

* `broadcaster` es el nombre del locutor que transmite en el momento.
* `show` es el nombre de su programa.

Estos últimos datos son de exclusiva responsabilidad del locutor, quien se
compromete a configurarlos correctamente en la aplicación que utilice para
transmitir.

## ¡Un momento! ¿Qué es toda esa majamama de letras que hay ahí? ##

Lo más probable es que estés refiriendo al campo cover. Pues bien, como dice ahí
arriba en la descripción, corresponde a la imagen del arte de la portada del
álbum, codificada usando [base64](http://es.wikipedia.org/wiki/Base64). Para tu
información, la imagen está en formato PNG, usando una
[paleta indexada](http://es.wikipedia.org/wiki/Profundidad_de_color#Color_indexado).

## Bien, ¿de adónde puedo acceder a la API? ##

Nuestra API está disponible en https://api.radiognu.org/

## ¿Cuáles son las funciones disponibles? ##

* :star:**`/cover/id/`**: Devuelve el arte de portada para el álbum
identificado por `id`. Ej: https://api.radiognu.org/cover/200/. Se devuelve un
JSON con el texto `{"cover":<texto>}`, donde `<texto>`es un archivo PNG con un
tamaño por defecto de 400 pixeles (modificable con `img_size`) que viene
codificado como base64.
* **`/cover/id.png`**: Devuelve el arte de portada para el álbum
identificado por `id`. Ej: https://api.radiognu.org/cover/200.png. El formato
devuelto es un archivo PNG (con Content-Type:image/png) con un tamaño por
defecto de 400 pixeles (modificable con `img_size`) que se sirve directamente
como tal.
* **`/preview/id/`**: Devuelve un trozo de la canción identificada por `id`. Ej:
https://api.radiognu.org/preview/1045/. Se devuelve un JSON con el texto
`{"preview":<texto>}`, donde `<texto>` es un archivo Vorbis de aproximadamente
10 segundos que viene codificado como base64.
* **`/preview/id.ogg`**: Devuelve un trozo de la canción identificada por
`id`. Ej: https://api.radiognu.org/preview/1045.ogg. El formato devuelto es un
archivo Vorbis (con Content-Type:audio/ogg) de aproximadamente 10 segundos que
se sirve directamente como tal.
* **`/catalog/`**: Devuelve la lista completa del repositorio musical de la radio.
* **`/album/id/`**: Devuelve la lista de canciones para el álbum identificado por
`id`. Ej: https://api.radiognu.org/album/236/.

## ¿Cuáles son los argumentos permitidos? ##

* **`/?img_size=tamaño`**: Usado para definir el tamaño del arte de portada del
álbum recibido (en pixeles). Acepta valores entre 50 a 400. Ej:
https://api.radiognu.org/?img_size=200
* **`/?no_cover`**: Devuelve la información de la API sin incluir el arte de
portada del álbum. Útil para programas y/o utilidades de sólo texto. Ej:
https://api.radiognu.org/?no_cover. Incluir este argumento hace que `img_size`
sea ignorado.

## ¿Tienes un ejemplo de cómo se usa? ##

Si, aquí tienes un ejemplo de como llamar de forma asíncrona a la API usando
[jQuery](http://jquery.com/):

```javascript
// Obtenemos los datos desde la API
$.getJSON("https://api.radiognu.org/", function (data) {
    // Una vez finalizada la carga, se puede comenzar a procesar los datos, por ej:
    console.log(data.artist);
    // Y si quieres agregar el arte de portada, lo puedes hacer así, por ej:
    $("<img/>").attr('src', data.cover);
});
```

Aquí, otro ejemplo, esta vez usando PHP:

```php
<?php
    // Obtenemos los datos desde la API
    $data = json_decode(file_get_contents("https://api.radiognu.org/"));
    // Una vez finalizada la carga, se puede comenzar a procesar los datos, por ej:
    echo $data->artist;
    // Y si quieres agregar el arte de portada, lo puedes hacer así, por ej:
    echo "<img src='".$data->cover."' alt='' />";
?>
```

Y aquí, otro ejemplo, usando Python:

```python
# -*- encoding: utf-8 -*-
import requests

# Obtenemos los datos desde la API
r = requests.get('https://api.radiognu.org/')
# Una vez finalizada la carga, se puede comenzar a procesar los datos, por ej:
print r.json()["artist"].encode("utf-8")
# Y si quieres agregar el arte de portada, lo puedes hacer así, por ej:
from PIL import Image
import base64
try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO
image = Image.open(StringIO(base64.b64decode(r.json()["cover"].split(",")[1])))
image.show()

```

## ¿Algo más? ##

Si. Esta documentación está en desarrollo, así como la API, por lo que cualquier
consulta de lo que no aparezca aquí, por favor la puedes hacer llegar por
nuestro canal de IRC, o agregar una incidencia en éste repositorio. Gracias de
antemano por tus comentarios.
